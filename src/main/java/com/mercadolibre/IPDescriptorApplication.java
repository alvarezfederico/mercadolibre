package com.mercadolibre;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.cache.RedisCache;
import com.mercadolibre.configuration.IPDescriptorConfiguration;
import com.mercadolibre.descriptor.IPDescriptor;
import com.mercadolibre.distance.DistanceCalculator;
import com.mercadolibre.http.HttpClient;
import com.mercadolibre.metric.IPDescriptorMetrics;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.http.CountryAdapter;
import com.mercadolibre.resource.IPDescriptorResource;
import com.mercadolibre.store.CountryStore;
import com.mercadolibre.store.IPStore;
import com.mercadolibre.store.RatesStore;
import com.mercadolibre.time.TimeProvider;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class IPDescriptorApplication extends Application<IPDescriptorConfiguration> {

    public static void main(String[] args) throws Exception {
        new IPDescriptorApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<IPDescriptorConfiguration> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<IPDescriptorConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(IPDescriptorConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    @Override
    public void run(IPDescriptorConfiguration configuration,
                    Environment environment) {

        HttpClient httpClient = new HttpClient();
        DistanceCalculator distanceCalculator = new DistanceCalculator();
        IPDescriptorMetrics metrics = new IPDescriptorMetrics();

        TimeProvider timeprovider = new TimeProvider();
        timeprovider.set(TimeProvider.getSystemTimeProvider());

        Gson generalParser = new Gson();
        Gson httpParser = new GsonBuilder().registerTypeAdapter(Country.class, new CountryAdapter(timeprovider)).create();

        Integer expirationTime = configuration.getExpirationTime();
        String redisHost = configuration.getRedisHost();
        Cache cache = new RedisCache(generalParser, redisHost, expirationTime);

        IPStore ipStore = new IPStore(httpClient);
        CountryStore countryStore = new CountryStore(httpClient, distanceCalculator, generalParser, cache);
        RatesStore ratesStore = new RatesStore(httpClient, generalParser, cache);
        IPDescriptor ipDescriptor = new IPDescriptor(ipStore, countryStore, ratesStore, metrics);

        // Register resources
        final IPDescriptorResource ipDescriptorResource = new IPDescriptorResource(ipDescriptor, httpParser, cache);

        environment.jersey().register(ipDescriptorResource);

    }
}
