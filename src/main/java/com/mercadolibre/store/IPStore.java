package com.mercadolibre.store;

import com.google.gson.JsonObject;
import com.mercadolibre.http.HttpClient;

public class IPStore {

    private final HttpClient httpClient;

    public IPStore(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public String retrieveIsoCode(String ip) {

        JsonObject root = httpClient.doGetRequest("api.ip2country.info/ip?", ip, true);

        String countryCode = root.get("countryCode").getAsString();

        return countryCode;
    }
}
