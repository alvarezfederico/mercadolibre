package com.mercadolibre.store;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.distance.DistanceCalculator;
import com.mercadolibre.http.HttpClient;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.CountryPosition;
import com.mercadolibre.model.Currency;
import com.mercadolibre.model.Language;

import java.lang.reflect.Type;
import java.util.List;

public class CountryStore {

    private final HttpClient httpClient;
    private final DistanceCalculator distanceCalculator;
    private final Gson parser;
    private final Cache cache;

    public CountryStore(HttpClient httpClient, DistanceCalculator distanceCalculator, Gson parser, Cache cache) {
        this.httpClient = httpClient;
        this.distanceCalculator = distanceCalculator;
        this.parser = parser;
        this.cache = cache;
    }

    public Country fillCountryInformation(String isoCode) {

        if (this.cache.countryExist(isoCode)) {
            return this.cache.retrieveCountryByIsoCode(isoCode);
        }

        JsonObject root = httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", isoCode, true);

        JsonArray languagesAsJSON = root.getAsJsonArray("languages");
        JsonArray currenciesAsJSON = root.getAsJsonArray("currencies");
        JsonArray timeZoneAsJSON = root.getAsJsonArray("timezones");
        JsonArray latlngAsJson = root.getAsJsonArray("latlng");
        String countryName = root.get("name").getAsString();

        Type languageType = new TypeToken<List<Language>>() {
        }.getType();

        Type currencyType = new TypeToken<List<Currency>>() {
        }.getType();

        Type stringType = new TypeToken<List<String>>() {
        }.getType();

        List<Language> languages = parser.fromJson(languagesAsJSON, languageType);
        List<Currency> currencies = parser.fromJson(currenciesAsJSON, currencyType);
        List<String> timeZone = parser.fromJson(timeZoneAsJSON, stringType);
        List<String> latLang = parser.fromJson(latlngAsJson, stringType);

        CountryPosition countryPosition = new CountryPosition(new Double(latLang.get(0)), new Double(latLang.get(1)));

        Country country = new Country(countryName, isoCode);
        country.addLanguages(languages);
        country.addTimezone(timeZone);
        country.addCurrencies(currencies);
        country.addCountryPosition(countryPosition);

        Double distance = this.distanceCalculator.calculateDistanceToArgentina(country.getPosition());
        country.distanceToArgentina(distance);

        this.cache.saveCountryByIsoCode(isoCode, country);

        return country;
    }
}
