package com.mercadolibre.store;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.http.HttpClient;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.Currency;
import com.mercadolibre.model.Rate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class RatesStore {

    private final HttpClient httpClient;
    private final Gson parser;
    private final Cache cache;

    public RatesStore(HttpClient httpClient, Gson parser, Cache cache) {
        this.httpClient = httpClient;
        this.parser = parser;
        this.cache = cache;
    }

    public List<Rate> retrieveMoneyInformation(Country country) {

        String currenciesSymbol = country.getCurrencies().stream().map(Currency::getCode).collect(Collectors.joining(","));

        if (this.cache.rateExist(currenciesSymbol)) {
            return this.cache.retrieveRate(currenciesSymbol);
        }

        JsonObject root = httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", currenciesSymbol, false);

        JsonObject ratesAsJson = root.getAsJsonObject("rates");

        HashMap<String, Double> rateMap = parser.fromJson(ratesAsJson, HashMap.class);

        List<Rate> rates = new ArrayList<>();

        country.getCurrencies().forEach(currency -> {
            Rate rate = new Rate(currency, BigDecimal.valueOf(rateMap.get(currency.getCode())));
            rates.add(rate);
        });

        this.cache.saveRate(currenciesSymbol, rates);

        return rates;
    }
}
