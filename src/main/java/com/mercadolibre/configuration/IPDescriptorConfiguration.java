package com.mercadolibre.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class IPDescriptorConfiguration extends Configuration {

    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    @JsonProperty("expiration_time")
    public Integer expirationTime;

    @JsonProperty("redis_host")
    public String redisHost;

    public Integer getExpirationTime() {
        return expirationTime;
    }

    public String getRedisHost() {
        return redisHost;
    }

}
