package com.mercadolibre.validator;

import java.util.regex.Pattern;

public class IpValidator {

    private final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    public boolean validate(final String ip) {
        return PATTERN.matcher(ip).matches();
    }
}
