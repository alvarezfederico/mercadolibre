package com.mercadolibre.cache;

import com.mercadolibre.model.Country;
import com.mercadolibre.model.Rate;

import java.util.List;

public interface Cache {
    Boolean countryExist(String key);

    Boolean ipExist(String key);

    Boolean rateExist(String key);

    String saveCountryByIsoCode(String key, Country country);

    String saveCountryByIp(String key, Country value);

    String saveRate(String key, List<Rate> rates);

    Country retrieveCountryByIsoCode(String key);

    Country retrieveCountryByIp(String key);

    List<Rate> retrieveRate(String key);

}
