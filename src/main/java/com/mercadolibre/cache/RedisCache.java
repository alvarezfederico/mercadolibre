package com.mercadolibre.cache;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.Rate;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

import java.util.List;

public class RedisCache implements Cache {

    public static final String IP_TABLE = "ml/ip#";
    public static final String COUNTRY_TABLE = "ml/country#";
    public static final String RATE_TABLE = "ml/rate#";
    private final Gson parser;

    private Jedis jedis;
    private SetParams params;

    public RedisCache(Gson parser, String host, Integer expirationTime) {
        this.parser = parser;
        jedis = new Jedis(host);
        params = SetParams.setParams().ex(expirationTime);
    }

    @Override
    public Boolean countryExist(String key) {
        return keyExist(COUNTRY_TABLE, key);
    }

    @Override
    public Boolean ipExist(String key) {
        return keyExist(IP_TABLE, key);
    }

    @Override
    public Boolean rateExist(String key) {
        return keyExist(RATE_TABLE, key);
    }

    @Override
    public String saveCountryByIsoCode(String key, Country country) {
        String countryAsJson = this.parser.toJson(country);
        return save(COUNTRY_TABLE, key, countryAsJson);
    }

    @Override
    public String saveCountryByIp(String key, Country value) {
        String countryAsJson = this.parser.toJson(value);
        return save(IP_TABLE, key, countryAsJson);
    }

    @Override
    public String saveRate(String key, List<Rate> rates) {
        String ratesAsJson = this.parser.toJson(rates);
        return save(RATE_TABLE, key, ratesAsJson);
    }

    @Override
    public Country retrieveCountryByIsoCode(String key) {
        return this.parser.fromJson(retrieveKey(COUNTRY_TABLE, key), Country.class);
    }

    @Override
    public Country retrieveCountryByIp(String key) {
        return this.parser.fromJson(retrieveKey(IP_TABLE, key), Country.class);
    }

    @Override
    public List<Rate> retrieveRate(String key) {
        String rateJson = retrieveKey(RATE_TABLE, key);
        return this.parser.fromJson(rateJson, new TypeToken<List<Rate>>() {
        }.getType());
    }

    private Boolean keyExist(String prefix, String key) {
        return jedis.exists(prefix + key);
    }

    private String retrieveKey(String prefix, String key) {
        if (!jedis.exists(prefix + key)) {
            throw new RuntimeException("Ip doesn't exist");
        }
        return jedis.get(prefix + key);
    }

    private String save(String prefix, String key, String value) {
        return jedis.set(prefix + key, value, params);
    }


}
