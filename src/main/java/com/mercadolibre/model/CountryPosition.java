package com.mercadolibre.model;

public class CountryPosition {

    private final Double latitude;
    private final Double longitude;

    public CountryPosition(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
