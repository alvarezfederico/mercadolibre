package com.mercadolibre.model;

public class Metric {

    private Double maxDistance;
    private Double minDistance;
    private Double avgDistance;

    public Metric(Double maxDistance, Double minDistance, Double avgDistance) {
        this.maxDistance = maxDistance;
        this.minDistance = minDistance;
        this.avgDistance = avgDistance;
    }

    public Double getMaxDistance() {
        return maxDistance;
    }

    public Double getMinDistance() {
        return minDistance;
    }

    public Double getAvgDistance() {
        return avgDistance;
    }
}
