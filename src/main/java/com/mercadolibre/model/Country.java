package com.mercadolibre.model;

import java.util.List;

public class Country {

    private String name;
    private String isoCode;
    private List<Language> languages;
    private List<String> timezone;
    private List<Currency> currencies;
    private List<Rate> rates;
    private CountryPosition countryPosition;
    private Double distanceToArgentina;

    public Country(String name, String isoCode) {
        this.name = name;
        this.isoCode = isoCode;
    }

    public String getName() {
        return name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void addLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public void addTimezone(List<String> timezone) {
        this.timezone = timezone;
    }

    public void addCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public List<String> getTimezone() {
        return timezone;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void addRates(List<Rate> rates) {
        this.rates = rates;
    }

    public void addCountryPosition(CountryPosition countryPosition) {
        this.countryPosition = countryPosition;
    }

    public CountryPosition getPosition() {
        return countryPosition;
    }

    public void distanceToArgentina(Double distanceToArgentina) {
        this.distanceToArgentina = distanceToArgentina;
    }

    public Double getDistanceToArgentina() {
        return distanceToArgentina;
    }
}
