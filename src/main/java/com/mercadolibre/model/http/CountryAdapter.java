package com.mercadolibre.model.http;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.Language;
import com.mercadolibre.time.TimeProvider;

import java.lang.reflect.Type;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CountryAdapter implements JsonSerializer<Country> {


    private final TimeProvider timeprovider;

    public CountryAdapter(TimeProvider timeprovider) {
        this.timeprovider = timeprovider;
    }

    @Override
    public JsonElement serialize(Country country, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("name", country.getName());
        obj.addProperty("iso_code", country.getIsoCode());

        JsonArray languages = new JsonArray();
        country.getLanguages().stream().map(Language::getName).forEach(language -> languages.add(language));
        obj.add("languages", languages);

        JsonArray times = new JsonArray();
        country.getTimezone().stream().forEach(timezone -> {
            ZoneId zoneId = ZoneId.of(timezone);
            ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(timeprovider.now(), zoneId);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss [z]");
            times.add(zonedDateTime.format(formatter));
        });
        obj.add("times", times);


        obj.addProperty("distance_to_buenos_aires", country.getDistanceToArgentina());

        JsonArray currency = new JsonArray();
        country.getRates().stream().forEach(rate -> {
            JsonObject currencyJSON = new JsonObject();
            currencyJSON.addProperty("currency_name", rate.getCurrency().getName());
            currencyJSON.addProperty("currency_rate", rate.getRate().doubleValue());
            currency.add(currencyJSON);
        });
        obj.add("currencies", currency);

        return obj;
    }

}
