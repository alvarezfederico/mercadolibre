package com.mercadolibre.model;

import java.math.BigDecimal;

public class Rate {

    private Currency currency;

    private BigDecimal rate;

    public Rate(Currency currency, BigDecimal rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getRate() {
        return rate;
    }
}
