package com.mercadolibre.resource;

import com.google.gson.Gson;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.descriptor.IPDescriptor;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.Metric;
import com.mercadolibre.validator.IpValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.status;

@Path(value = "v1")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "IP descriptor")
public class IPDescriptorResource {

    private final IPDescriptor ipDescriptor;
    private final Gson httpParser;
    private final Cache cache;

    public IPDescriptorResource(IPDescriptor ipDescriptor, Gson httpParser, Cache cache) {
        this.ipDescriptor = ipDescriptor;
        this.httpParser = httpParser;
        this.cache = cache;
    }

    @GET
    @Path("description/{ip}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Retrieve Ip information",
            response = Response.class,
            responseContainer = "List")
    public Response retrieveIpInformation(@PathParam("ip") String ip) {

        IpValidator ipValidator = new IpValidator();
        if (!ipValidator.validate(ip)) {
            return status(Response.Status.PRECONDITION_FAILED).entity(this.httpParser.toJson("Ip is not well formed")).build();
        }

        Country country;

        if (this.cache.ipExist(ip)) {
            country = this.cache.retrieveCountryByIp(ip);
        } else {
            country = this.ipDescriptor.retrieveIPInformation(ip);
            this.cache.saveCountryByIp(ip, country);
        }

        this.ipDescriptor.updateMetrics(country);

        return Response.status(Response.Status.OK).entity(this.httpParser.toJson(country)).build();
    }

    @GET
    @Path("metrics")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Retrieve service metrics",
            response = Response.class,
            responseContainer = "List")
    public Response metrics() {
        Metric metrics = this.ipDescriptor.getMetrics();
        return Response.status(Response.Status.OK).entity(metrics).build();
    }
}
