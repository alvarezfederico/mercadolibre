package com.mercadolibre.time;

import java.time.Instant;

public class TimeProvider {

    private static TimeProvider INSTANCE;

    public static void set(TimeProvider timeProvider) {
        INSTANCE = timeProvider;
    }

    public static TimeProvider getSystemTimeProvider() {
        return new TimeProvider();
    }

    public Instant now() {
        return INSTANCE.nowInstant();
    }

    protected Instant nowInstant() {
        return Instant.now();
    }
}
