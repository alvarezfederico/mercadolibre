package com.mercadolibre.descriptor;

import com.mercadolibre.metric.IPDescriptorMetrics;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.Metric;
import com.mercadolibre.model.Rate;
import com.mercadolibre.store.CountryStore;
import com.mercadolibre.store.IPStore;
import com.mercadolibre.store.RatesStore;

import java.util.List;

public class IPDescriptor {

    private final IPDescriptorMetrics metrics;
    private final CountryStore countryStore;
    private final RatesStore ratesStore;
    private final IPStore ipStore;

    public IPDescriptor(IPStore ipStore, CountryStore countryStore, RatesStore ratesStore, IPDescriptorMetrics metrics) {
        this.ipStore = ipStore;
        this.metrics = metrics;
        this.countryStore = countryStore;
        this.ratesStore = ratesStore;
    }

    public Country retrieveIPInformation(String ip) {

        String isoCode = ipStore.retrieveIsoCode(ip);
        Country country = countryStore.fillCountryInformation(isoCode);
        List<Rate> rates = ratesStore.retrieveMoneyInformation(country);
        country.addRates(rates);

        return country;
    }

    public Metric getMetrics() {
        Metric metric = new Metric(this.metrics.getMaxDistance(), this.metrics.getMinDistance(), this.metrics.getAverageDistance());
        return metric;
    }

    public void updateMetrics(Country country) {
        this.metrics.registerNewDistance(country.getDistanceToArgentina());
    }


}
