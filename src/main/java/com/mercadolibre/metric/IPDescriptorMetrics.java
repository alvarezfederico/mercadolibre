package com.mercadolibre.metric;

public class IPDescriptorMetrics {

    private Double maxDistance;
    private Double minDistance;
    private Double amountOfCalls;
    private Double average = 0D;

    public IPDescriptorMetrics() {
        this.maxDistance = 0D;
        this.minDistance = Double.MAX_VALUE;
        this.amountOfCalls = 0D;
    }

    public void registerNewDistance(Double distance) {
        if (distance > maxDistance) {
            maxDistance = distance;
        }
        if (distance < minDistance) {
            minDistance = distance;
        }
        /**
         * Solution taken from https://stackoverflow.com/a/16757630/1903925
         */
        amountOfCalls++;
        this.average = average * (amountOfCalls - 1) / amountOfCalls + distance / amountOfCalls;
    }

    public Double getMaxDistance() {
        return maxDistance;
    }

    public Double getMinDistance() {
        return minDistance;
    }

    public Double getAverageDistance() {
        return this.average;
    }

}
