package com.mercadolibre.http;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

public class HttpClient {

    public JsonObject doGetRequest(String endpoint, String parameter, boolean https) {
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

            JsonObject root = performGETRequest(client, endpoint, parameter, https);

            return root;

        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private JsonObject performGETRequest(CloseableHttpClient client, String endpoint, String param, Boolean https) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        if (https) {
            builder.setScheme("https");
        } else {
            builder.setScheme("http");
        }
        builder.setHost(endpoint + param);
        HttpGet httpGet = new HttpGet(builder.build());
        CloseableHttpResponse response = client.execute(httpGet);
        String bodyAsString = EntityUtils.toString(response.getEntity());

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(bodyAsString);
        return root.getAsJsonObject();
    }
}
