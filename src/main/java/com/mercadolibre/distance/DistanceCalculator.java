package com.mercadolibre.distance;

import com.mercadolibre.model.CountryPosition;

/**
 * The baseline of this code is from: https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude/16794680#16794680
 * with some slightly different
 */
public class DistanceCalculator {

    private Double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        Double theta = lon1 - lon2;
        Double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        //Convert to kilometers
        dist = dist * 1.609344;

        return dist;
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public Double calculateDistance(CountryPosition countryFrom, CountryPosition countryTo) {
        return calculateDistance(countryFrom.getLatitude(), countryFrom.getLongitude(), countryTo.getLatitude(), countryTo.getLongitude());
    }

    public Double calculateDistanceToArgentina(CountryPosition countryFrom) {
        CountryPosition argentina = new CountryPosition(new Double("-34"), new Double("-64"));
        return calculateDistance(countryFrom.getLatitude(), countryFrom.getLongitude(), argentina.getLatitude(), argentina.getLongitude());
    }

}
