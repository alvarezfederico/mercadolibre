package com.mercadolibre.cache;

import com.mercadolibre.model.Country;
import com.mercadolibre.model.Rate;

import java.util.List;

public class DummyCache implements Cache {

    @Override
    public Boolean countryExist(String key) {
        return false;
    }

    @Override
    public Boolean ipExist(String key) {
        return false;
    }

    @Override
    public Boolean rateExist(String key) {
        return false;
    }

    @Override
    public String saveCountryByIsoCode(String key, Country country) {
        return null;
    }

    @Override
    public String saveCountryByIp(String key, Country value) {
        return null;
    }

    @Override
    public String saveRate(String key, List<Rate> rates) {
        return null;
    }

    @Override
    public Country retrieveCountryByIsoCode(String key) {
        return null;
    }

    @Override
    public Country retrieveCountryByIp(String key) {
        return null;
    }

    @Override
    public List<Rate> retrieveRate(String key) {
        return null;
    }
}
