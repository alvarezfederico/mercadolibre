package com.mercadolibre.descriptor;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.cache.DummyCache;
import com.mercadolibre.distance.DistanceCalculator;
import com.mercadolibre.http.HttpClient;
import com.mercadolibre.metric.IPDescriptorMetrics;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.CountryPosition;
import com.mercadolibre.model.Currency;
import com.mercadolibre.model.Language;
import com.mercadolibre.model.Rate;
import com.mercadolibre.store.CountryStore;
import com.mercadolibre.store.IPStore;
import com.mercadolibre.store.RatesStore;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class StoreTest {

    IPDescriptor ipDescriptor;
    IPStore ipStore;
    CountryStore countryStore;
    RatesStore ratesStore;

    @Before
    public void prepare() {
        HttpClient httpClient = createHttpClientMocked();
        DistanceCalculator distanceCalculator = new DistanceCalculator();
        IPDescriptorMetrics metrics = new IPDescriptorMetrics();
        Gson parser = new Gson();
        Cache dummyCache = new DummyCache();
        ipStore = new IPStore(httpClient);
        countryStore = new CountryStore(httpClient, distanceCalculator, parser, dummyCache);
        ratesStore = new RatesStore(httpClient, parser, dummyCache);
        ipDescriptor = new IPDescriptor(ipStore, countryStore, ratesStore, metrics);
    }

    @Test
    public void ipFromArgentina() {


        String isoCode = ipStore.retrieveIsoCode("181.171.12.23");

        assertThat(isoCode).isEqualTo("AR");

        Country country = countryStore.fillCountryInformation(isoCode);

        assertThat(country.getLanguages()).hasSize(2).extracting(Language::getName).containsExactly("Spanish", "Guaraní");
        assertThat(country.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("Argentine peso");
        assertThat(country.getTimezone()).hasSize(1).containsExactly("UTC-03:00");
        assertThat(country.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("-34.0"));
        assertThat(country.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("-64.0"));

        List<Rate> rates = ratesStore.retrieveMoneyInformation(country);

        assertThat(rates).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("50.453592"));
    }

    @Test
    public void ipFromGermany() {

        String isoCode = ipStore.retrieveIsoCode("5.6.7.8");

        assertThat(isoCode).isEqualTo("DE");

        Country country = countryStore.fillCountryInformation(isoCode);

        assertThat(country.getLanguages()).hasSize(1).extracting(Language::getName).containsExactly("German");
        assertThat(country.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("Euro");
        assertThat(country.getTimezone()).hasSize(1).containsExactly("UTC+01:00");
        assertThat(country.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("51.0"));
        assertThat(country.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("9.0"));


        List<Rate> rates = ratesStore.retrieveMoneyInformation(country);

        assertThat(rates).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("1.0"));
    }

    @Test
    public void ipFromUSA() {

        String isoCode = ipStore.retrieveIsoCode("4.5.6.7");

        assertThat(isoCode).isEqualTo("US");

        Country country = countryStore.fillCountryInformation(isoCode);

        assertThat(country.getLanguages()).hasSize(1).extracting(Language::getName).containsExactly("English");

        assertThat(country.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("United States dollar");
        assertThat(country.getTimezone()).hasSize(11).containsExactly("UTC-12:00",
                "UTC-11:00",
                "UTC-10:00",
                "UTC-09:00",
                "UTC-08:00",
                "UTC-07:00",
                "UTC-06:00",
                "UTC-05:00",
                "UTC-04:00",
                "UTC+10:00",
                "UTC+12:00");
        assertThat(country.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("38.0"));
        assertThat(country.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("-97.0"));

        List<Rate> rates = ratesStore.retrieveMoneyInformation(country);

        assertThat(rates).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("1.12205"));
    }

    private HttpClient createHttpClientMocked() {
        HttpClient httpClient = Mockito.mock(HttpClient.class);

        //For ip: 181.171.12.23 --> Argentina
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "181.171.12.23", true))
                .thenReturn(readJSON("src/test/resources/argentina-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "AR", true))
                .thenReturn(readJSON("src/test/resources/argentina-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "ARS", false))
                .thenReturn(readJSON("src/test/resources/argentina-money.json"));

        // From german IP --> 5.6.7.8
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "5.6.7.8", true))
                .thenReturn(readJSON("src/test/resources/german-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "DE", true))
                .thenReturn(readJSON("src/test/resources/german-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "EUR", false))
                .thenReturn(readJSON("src/test/resources/german-money.json"));

        // From usa IP --> 5.6.7.8
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "4.5.6.7", true))
                .thenReturn(readJSON("src/test/resources/usa-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "US", true))
                .thenReturn(readJSON("src/test/resources/usa-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "USD", false))
                .thenReturn(readJSON("src/test/resources/usa-money.json"));
        return httpClient;
    }

    private JsonObject readJSON(String filePath) {
        StringBuffer json = new StringBuffer();
        Path resourceDirectory = Paths.get(filePath);
        try {
            Files.lines(resourceDirectory).forEach(line -> json.append(line));
        } catch (IOException e) {
            throw new RuntimeException("Error reading the file");
        }

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(json.toString());
        return root.getAsJsonObject();
    }

}
