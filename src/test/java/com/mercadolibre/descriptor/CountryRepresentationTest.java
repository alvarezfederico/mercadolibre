package com.mercadolibre.descriptor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.cache.DummyCache;
import com.mercadolibre.distance.DistanceCalculator;
import com.mercadolibre.http.HttpClient;
import com.mercadolibre.metric.IPDescriptorMetrics;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.http.CountryAdapter;
import com.mercadolibre.store.CountryStore;
import com.mercadolibre.store.IPStore;
import com.mercadolibre.store.RatesStore;
import com.mercadolibre.time.TimeProvider;
import com.mercadolibre.time.TimeProviderAdjustable;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

public class CountryRepresentationTest {


    IPDescriptor ipDescriptor;
    TimeProvider timeProvider;
    TimeProviderAdjustable timeProviderAdjustable;

    @Before
    public void prepareIpDescriptor() {
        //Set timer
        timeProvider = new TimeProvider();
        timeProviderAdjustable = new TimeProviderAdjustable();

        LocalDate localDate = LocalDate.of(2019, 05, 13);
        LocalDateTime localDateTime = localDate.atTime(12, 00);

//        ZoneId zoneId = ZoneId.systemDefault();
        Instant fixedCurrentDate = localDateTime.atZone(ZoneOffset.UTC).toInstant();
        timeProviderAdjustable.setCurrentTime(fixedCurrentDate);

        timeProvider.set(timeProviderAdjustable);

        // Set ip descriptor
        HttpClient httpClient = createHttpClientMocked();
        DistanceCalculator distanceCalculator = new DistanceCalculator();
        IPDescriptorMetrics metrics = new IPDescriptorMetrics();
        Gson parser = new Gson();
        IPStore ipStore = new IPStore(httpClient);
        Cache dummyCache = new DummyCache();
        CountryStore countryStore = new CountryStore(httpClient, distanceCalculator, parser, dummyCache);
        RatesStore ratesStore = new RatesStore(httpClient, parser, dummyCache);
        ipDescriptor = new IPDescriptor(ipStore, countryStore, ratesStore, metrics);
    }

    @Test
    public void serialization() {
        Country ipFromGermany = ipDescriptor.retrieveIPInformation("5.6.7.8");

        Gson parser = new GsonBuilder().registerTypeAdapter(Country.class, new CountryAdapter(timeProvider)).create();

        String json = parser.toJson(ipFromGermany);

        assertThat(json).isEqualTo(readJSON("src/test/resources/GermanIpResponse.json").toString());
    }

    private HttpClient createHttpClientMocked() {
        HttpClient httpClient = Mockito.mock(HttpClient.class);

        //For ip: 181.171.12.23 --> Argentina
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "181.171.12.23", true))
                .thenReturn(readJSON("src/test/resources/argentina-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "AR", true))
                .thenReturn(readJSON("src/test/resources/argentina-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "ARS", false))
                .thenReturn(readJSON("src/test/resources/argentina-money.json"));

        // From german IP --> 5.6.7.8
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "5.6.7.8", true))
                .thenReturn(readJSON("src/test/resources/german-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "DE", true))
                .thenReturn(readJSON("src/test/resources/german-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "EUR", false))
                .thenReturn(readJSON("src/test/resources/german-money.json"));

        // From usa IP --> 5.6.7.8
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "4.5.6.7", true))
                .thenReturn(readJSON("src/test/resources/usa-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "US", true))
                .thenReturn(readJSON("src/test/resources/usa-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "USD", false))
                .thenReturn(readJSON("src/test/resources/usa-money.json"));
        return httpClient;
    }

    private JsonObject readJSON(String filePath) {
        StringBuffer json = new StringBuffer();
        Path resourceDirectory = Paths.get(filePath);
        try {
            Files.lines(resourceDirectory).forEach(line -> json.append(line));
        } catch (IOException e) {
            throw new RuntimeException("Error reading the file");
        }

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(json.toString());
        return root.getAsJsonObject();
    }
}
