package com.mercadolibre.descriptor;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mercadolibre.cache.Cache;
import com.mercadolibre.cache.DummyCache;
import com.mercadolibre.distance.DistanceCalculator;
import com.mercadolibre.http.HttpClient;
import com.mercadolibre.metric.IPDescriptorMetrics;
import com.mercadolibre.model.Country;
import com.mercadolibre.model.CountryPosition;
import com.mercadolibre.model.Currency;
import com.mercadolibre.model.Language;
import com.mercadolibre.model.Metric;
import com.mercadolibre.model.Rate;
import com.mercadolibre.store.CountryStore;
import com.mercadolibre.store.IPStore;
import com.mercadolibre.store.RatesStore;
import org.assertj.core.data.Percentage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class IPDescriptorTest {

    IPDescriptor ipDescriptor;

    @Before
    public void prepareIpDescriptor() {
        HttpClient httpClient = createHttpClientMocked();
        DistanceCalculator distanceCalculator = new DistanceCalculator();
        IPDescriptorMetrics metrics = new IPDescriptorMetrics();
        Gson parser = new Gson();
        Cache dummyCache = new DummyCache();
        IPStore ipStore = new IPStore(httpClient);
        CountryStore countryStore = new CountryStore(httpClient, distanceCalculator, parser, dummyCache);
        RatesStore ratesStore = new RatesStore(httpClient, parser, dummyCache);
        ipDescriptor = new IPDescriptor(ipStore, countryStore, ratesStore, metrics);
    }

    @Test
    public void callFromArgGermanyAndUSA() {

        //Call from Argentina
        Country ipFromArgentina = ipDescriptor.retrieveIPInformation("181.171.12.23");

        assertThat(ipFromArgentina).extracting(Country::getName).isEqualTo("Argentina");
        assertThat(ipFromArgentina).extracting(Country::getIsoCode).isEqualTo("AR");
        assertThat(ipFromArgentina.getLanguages()).hasSize(2).extracting(Language::getName).containsExactly("Spanish", "Guaraní");
        assertThat(ipFromArgentina.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("Argentine peso");
        assertThat(ipFromArgentina.getTimezone()).hasSize(1).containsExactly("UTC-03:00");
        assertThat(ipFromArgentina.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("-34.0"));
        assertThat(ipFromArgentina.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("-64.0"));
        assertThat(ipFromArgentina.getRates()).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("50.453592"));

        ipDescriptor.updateMetrics(ipFromArgentina);
        Metric metric = ipDescriptor.getMetrics();
        assertThat(metric).extracting(Metric::getMaxDistance).isEqualTo(0.0);
        assertThat(metric).extracting(Metric::getMinDistance).isEqualTo(0.0);
        assertThat(metric).extracting(Metric::getAvgDistance).isEqualTo(0.0);

        //Call from germany
        Country ipFromGermany = ipDescriptor.retrieveIPInformation("5.6.7.8");

        assertThat(ipFromGermany).extracting(Country::getName).isEqualTo("Germany");
        assertThat(ipFromGermany).extracting(Country::getIsoCode).isEqualTo("DE");
        assertThat(ipFromGermany.getLanguages()).hasSize(1).extracting(Language::getName).containsExactly("German");
        assertThat(ipFromGermany.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("Euro");
        assertThat(ipFromGermany.getTimezone()).hasSize(1).containsExactly("UTC+01:00");
        assertThat(ipFromGermany.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("51.0"));
        assertThat(ipFromGermany.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("9.0"));
        assertThat(ipFromGermany.getRates()).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("1.0"));

        ipDescriptor.updateMetrics(ipFromGermany);
        metric = ipDescriptor.getMetrics();

        /**
         * By google de difference is: 12257km (https://www.distancefromto.net/distance-from-argentina-to-germany)
         * I think the differences relies on the coordinates:
         * Arg: -38.41610, -63.61667 instead of -34, -64
         * Germany: 51.16569,10.45153 instead 51, 9
         *
         * Also the web is taking into account the Altitude, so I beleive that my number is pretty accurate.
         */
        assertThat(metric.getMaxDistance()).isCloseTo(11828, Percentage.withPercentage(1));
        assertThat(metric.getMinDistance()).isEqualTo(0.0);
        assertThat(metric.getAvgDistance()).isCloseTo(5914, Percentage.withPercentage(1));

        //Call from USA
        Country ipFromUsa = ipDescriptor.retrieveIPInformation("4.5.6.7");

        assertThat(ipFromUsa).extracting(Country::getName).isEqualTo("United States of America");
        assertThat(ipFromUsa).extracting(Country::getIsoCode).isEqualTo("US");
        assertThat(ipFromUsa.getLanguages()).hasSize(1).extracting(Language::getName).containsExactly("English");
        assertThat(ipFromUsa.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("United States dollar");
        assertThat(ipFromUsa.getTimezone()).hasSize(11).containsExactly("UTC-12:00",
                "UTC-11:00",
                "UTC-10:00",
                "UTC-09:00",
                "UTC-08:00",
                "UTC-07:00",
                "UTC-06:00",
                "UTC-05:00",
                "UTC-04:00",
                "UTC+10:00",
                "UTC+12:00");
        assertThat(ipFromUsa.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("38.0"));
        assertThat(ipFromUsa.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("-97.0"));
        assertThat(ipFromUsa.getRates()).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("1.12205"));

        ipDescriptor.updateMetrics(ipFromUsa);
        metric = ipDescriptor.getMetrics();
        assertThat(metric.getMaxDistance()).isCloseTo(11828, Percentage.withPercentage(1));
        assertThat(metric.getMinDistance()).isEqualTo(0.0);
        assertThat(metric.getAvgDistance()).isCloseTo(6843, Percentage.withPercentage(1));

    }

    @Test
    public void callFromGermanyAndUSA() {

        //Call from germany
        Country ipFromGermany = ipDescriptor.retrieveIPInformation("5.6.7.8");

        assertThat(ipFromGermany).extracting(Country::getName).isEqualTo("Germany");
        assertThat(ipFromGermany).extracting(Country::getIsoCode).isEqualTo("DE");
        assertThat(ipFromGermany.getLanguages()).hasSize(1).extracting(Language::getName).containsExactly("German");
        assertThat(ipFromGermany.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("Euro");
        assertThat(ipFromGermany.getTimezone()).hasSize(1).containsExactly("UTC+01:00");
        assertThat(ipFromGermany.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("51.0"));
        assertThat(ipFromGermany.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("9.0"));
        assertThat(ipFromGermany.getRates()).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("1.0"));

        this.ipDescriptor.updateMetrics(ipFromGermany);

        Metric metric = ipDescriptor.getMetrics();

        assertThat(metric.getMaxDistance()).isCloseTo(11828, Percentage.withPercentage(1));
        assertThat(metric.getMinDistance()).isCloseTo(11828, Percentage.withPercentage(1));
        assertThat(metric.getAvgDistance()).isCloseTo(11828, Percentage.withPercentage(1));

        //Call from USA
        Country ipFromUsa = ipDescriptor.retrieveIPInformation("4.5.6.7");

        assertThat(ipFromUsa).extracting(Country::getName).isEqualTo("United States of America");
        assertThat(ipFromUsa).extracting(Country::getIsoCode).isEqualTo("US");
        assertThat(ipFromUsa.getLanguages()).hasSize(1).extracting(Language::getName).containsExactly("English");
        assertThat(ipFromUsa.getCurrencies()).hasSize(1).extracting(Currency::getName).containsExactly("United States dollar");
        assertThat(ipFromUsa.getTimezone()).hasSize(11).containsExactly("UTC-12:00",
                "UTC-11:00",
                "UTC-10:00",
                "UTC-09:00",
                "UTC-08:00",
                "UTC-07:00",
                "UTC-06:00",
                "UTC-05:00",
                "UTC-04:00",
                "UTC+10:00",
                "UTC+12:00");
        assertThat(ipFromUsa.getPosition()).extracting(CountryPosition::getLatitude).isEqualTo(new Double("38.0"));
        assertThat(ipFromUsa.getPosition()).extracting(CountryPosition::getLongitude).isEqualTo(new Double("-97.0"));
        assertThat(ipFromUsa.getRates()).hasSize(1).extracting(Rate::getRate).containsExactly(new BigDecimal("1.12205"));

        this.ipDescriptor.updateMetrics(ipFromUsa);
        metric = ipDescriptor.getMetrics();
        assertThat(metric.getMaxDistance()).isCloseTo(11828, Percentage.withPercentage(1));
        assertThat(metric.getMinDistance()).isCloseTo(8700, Percentage.withPercentage(1));
        assertThat(metric.getAvgDistance()).isCloseTo(10264, Percentage.withPercentage(1));

    }

    private HttpClient createHttpClientMocked() {
        HttpClient httpClient = Mockito.mock(HttpClient.class);

        //For ip: 181.171.12.23 --> Argentina
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "181.171.12.23", true))
                .thenReturn(readJSON("src/test/resources/argentina-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "AR", true))
                .thenReturn(readJSON("src/test/resources/argentina-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "ARS", false))
                .thenReturn(readJSON("src/test/resources/argentina-money.json"));

        // From german IP --> 5.6.7.8
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "5.6.7.8", true))
                .thenReturn(readJSON("src/test/resources/german-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "DE", true))
                .thenReturn(readJSON("src/test/resources/german-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "EUR", false))
                .thenReturn(readJSON("src/test/resources/german-money.json"));

        // From usa IP --> 5.6.7.8
        Mockito.when(httpClient.doGetRequest("api.ip2country.info/ip?", "4.5.6.7", true))
                .thenReturn(readJSON("src/test/resources/usa-ip.json"));

        Mockito.when(httpClient.doGetRequest("restcountries.eu/rest/v2/alpha/", "US", true))
                .thenReturn(readJSON("src/test/resources/usa-country.json"));

        Mockito.when(httpClient.doGetRequest("data.fixer.io/api/latest?access_key=0cd7f08d6016c5b5b15d9fd09cd5e3de&symbols=", "USD", false))
                .thenReturn(readJSON("src/test/resources/usa-money.json"));
        return httpClient;
    }

    private JsonObject readJSON(String filePath) {
        StringBuffer json = new StringBuffer();
        Path resourceDirectory = Paths.get(filePath);
        try {
            Files.lines(resourceDirectory).forEach(line -> json.append(line));
        } catch (IOException e) {
            throw new RuntimeException("Error reading the file");
        }

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(json.toString());
        return root.getAsJsonObject();
    }
}
