package com.mercadolibre.learning;

import com.mercadolibre.distance.DistanceCalculator;
import com.mercadolibre.model.CountryPosition;
import org.assertj.core.data.Percentage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MeasureDistanceLearning {

    /**
     * This scenario is taken from the assignment definition but there is a small difference.
     * I think for the sake of the test will be fine.
     * <p>
     * Expected result: 10270 kms (-34, -64) a (40, -4)
     * <p>
     * I've used this code: https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude/16794680#16794680
     * <p>
     * The result is not identical so my test check if a result is close to 10270km (1% diff)
     */
    @Test
    public void calculateDistanceFromSpainToArg() {

        DistanceCalculator distanceCalculator = new DistanceCalculator();
        CountryPosition argentina = new CountryPosition(new Double("-34"), new Double("-64"));
        CountryPosition spain = new CountryPosition(new Double("40"), new Double("-4"));

        Double distance = distanceCalculator.calculateDistance(argentina, spain);

        assertThat(distance).isCloseTo(10270, Percentage.withPercentage(1));
    }

}
