package com.mercadolibre.time;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

public class TimeProviderAdjustable extends TimeProvider {

    private Instant currentTime;

    public void setCurrentTime(Instant now) {
        this.currentTime = now;
    }

    public void advanceMinutes(long minutes) {
        this.currentTime = this.currentTime.plus(minutes, ChronoUnit.MINUTES);
    }

    @Override
    protected Instant nowInstant() {
        Clock fixedClock = Clock.fixed(currentTime, ZoneId.systemDefault());
        return Instant.now(fixedClock);
    }

}
