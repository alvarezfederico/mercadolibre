package com.mercadolibre;

import com.mercadolibre.metric.IPDescriptorMetrics;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MetricTest {

    @Test
    public void calculateSimpleDistances() {
        Double distance10 = 10D;
        Double distance20 = 20D;
        Double distance30 = 30D;

        IPDescriptorMetrics ipDescriptorMetrics = new IPDescriptorMetrics();

        ipDescriptorMetrics.registerNewDistance(distance10);
        assertThat(ipDescriptorMetrics.getMaxDistance()).isEqualTo(10D);
        assertThat(ipDescriptorMetrics.getMinDistance()).isEqualTo(10D);
        assertThat(ipDescriptorMetrics.getAverageDistance()).isEqualTo(10D);

        ipDescriptorMetrics.registerNewDistance(distance20);
        assertThat(ipDescriptorMetrics.getMaxDistance()).isEqualTo(20D);
        assertThat(ipDescriptorMetrics.getMinDistance()).isEqualTo(10D);
        assertThat(ipDescriptorMetrics.getAverageDistance()).isEqualTo(15D);

        ipDescriptorMetrics.registerNewDistance(distance30);
        assertThat(ipDescriptorMetrics.getMaxDistance()).isEqualTo(30D);
        assertThat(ipDescriptorMetrics.getMinDistance()).isEqualTo(10D);
        assertThat(ipDescriptorMetrics.getAverageDistance()).isEqualTo(20D);
    }
}
