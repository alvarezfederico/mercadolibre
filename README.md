# Ejercicio de programación

## Descripción

Construí una API simple con dos endpoints: 

- `v1/description/{ip}` GET method que recolecta informacíon acerca de una IP
- `v1/metrics` GET method que muestra las estadisticas de uso del primer endpoint

Decidí que los dos metodos fuesen get porque son idempotente, aunque tuve la duda en el caso del primero ya que estaba modificando 
el estado en mi server creando registros dentro de la cache y  metricas, pero a nivel negocio no esta generando un recurso nuevo.

## How to run

Utilicé docker compose entonces solo hay que correr: ` docker-compose build` && `docker-compose up`

Agregué swagger así que una forma fácil de explorar la app es entrando en: `http://localhost:8080/swagger` 

## Decisiones técnicas

Utilicé dropwizard como web application porque considero que es la unión de varios frameworks que están bien testeados y son standars:
Jetty,Jersey,logback,etc.

Usé redis como cache porque considero que es una opción simple y efectiva para solucionar el problema de caching.

Basé mi diseño en domain driven design, comparto la idea de usar un lenguaje obicuo entre todas las partes.

Usé tdd para crear mis objetos de dominio y mi lógica en general.

No me gusta crear dto para representar mi información entonces para retornar los resultados asociado a la `IP` uso un parser específico (`CountryAdapter`) 


## Testing

Por cuestiones de tiempo no cree todos los escenarios ideales para la aplicación, hice test unitarios para probar mi 
lógica principal. Pero me faltó test mas integrales: por ejemplo no estoy testeando la capa http, faltaria un test de ese estilo.
La cache tampoco: en mis test unitarios tengo una implementacion in memory que siempre devuelve false cuando se pide una key.
Tampoco hay muchos test de excepciones, como por ejemplo que pasa si la ip no respeta el formato adecuado.

Cree un provider del tiempo para no tener una dependencia directa con el sistema en el que reside la applicacion y ademas me permite 
testear de una manera mas elegante.

Utilice `assertj` como complemento de `junit assertions` porque me resulta mas semantico.


## Performance

Como posibles cuellos de botella encontré dos puntos claves:

- Metricas: implemente una solución o(1) para obtener el máximo, mínimo y promedio. Para el promedio utilicé un algoritmo que cito en el código.

- Multiples request a `/description`: Implementé una cache para salvar los valores de IP, Country y Rates. Definí la expiración
en un día para todas las keys, pero esto se puede cambiar facilmente y extender en caso necesario.

## Mejoras

- Por el momento las metricas se concentran en cada aplicación no son distribuidas, esto es fácil de resolver si se guardan en una base distribuida que podria ser el mismo redis
- En vez de generar una mutabilidad en el campo `Metric#average` guardar cada evento y recorrerlo para tener metricas más flexibles
poder calcular por período de tiempo,etc. También se podrian hacer los dos approachs para tener una metrica rápida y otra más "quereable"
- Testing mas exhaustivo, chequear red path, implementar redis in memory,etc.
- Configurar particularmente la expiración de cada cache, etc.
- Crear un endpoint para expirar registros de cache, etc.
- Implementar seguridad en los endpoints

