FROM maven:3.6.0-jdk-8-alpine

WORKDIR /opt

ADD src/ ./src/

ADD pom.xml ./

RUN mvn clean package

CMD ["java", "-jar", "target/ip.description-1.0-SNAPSHOT.jar", "server", "src/main/resources/configuration.yml"]

EXPOSE 8080
